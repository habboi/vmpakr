module Main where

import Debug.Trace
import qualified Data.ByteString               as B
import qualified Data.ByteString.Internal      as BI
import qualified Data.ByteString.Lazy          as BL
import qualified Data.ByteString.Lazy.Internal as BLI
import qualified Data.ByteString.Char8         as C
import Data.Binary.Put
import Data.Digest.Pure.MD5
import Data.Map as M
import System.Directory
import System.Environment
import System.FilePath
import System.IO

tocEntrySize = 296

type FileEntry = (Int, BL.ByteString)

data PakState
  = PakState { outFile :: Handle
             , fileMap :: M.Map FilePath FileEntry
             }

newPak fh = PakState { outFile = fh, fileMap = M.fromList [] }

addFileEntry fp fsize fbytes pak = pak { fileMap = M.insert fp (fsize, fbytes) (fileMap pak) }

processFile :: FilePath -> PakState -> IO PakState
processFile fp pak = do
  fh <- openFile fp ReadMode
  fsize <- hFileSize fh >>= pure . fromIntegral
  fbytes <- BL.hGet fh fsize
  putStrLn ("Adding file " ++ fp)
  return $ addFileEntry fp fsize fbytes pak

processDirEntries :: FilePath -> [FilePath] -> PakState -> IO PakState
processDirEntries dp [] pak = return pak
processDirEntries dp (fp : fps) pak = do
  let abspath = dp ++ [pathSeparator] ++ fp
  isdir <- doesDirectoryExist abspath
  pak' <- if isdir then processDir abspath pak else processFile abspath pak
  pak'' <- processDirEntries dp fps pak'
  return pak''

processDir :: FilePath -> PakState -> IO PakState
processDir fp pak = do
  ents <- listDirectory fp
  pak' <- processDirEntries fp ents pak
  return pak'

putFilesToc :: [(FilePath, FileEntry)] -> Int -> Put
putFilesToc [] _ = pure ()
putFilesToc ((fp, (fsize, fbytes)) : rs) offs = do
  let fpad = replicate (256 - length fp) '\0'
  putByteString $ C.pack fp                  -- write filename
  putByteString $ C.pack $ fpad              -- pad to filename 256 bytes
  putByteString $ C.pack $ show (md5 fbytes) -- write file md5 checksum
  putWord32le $ fromIntegral offs            -- write file offset
  putWord32le $ fromIntegral fsize           -- write file size
  putFilesToc rs (offs + fsize)

putFilesContent :: [(FilePath, FileEntry)] -> Put
putFilesContent [] = pure ()
putFilesContent ((fp, (fsize, fbytes)) : rs) = do
  putByteString $ (B.concat . BL.toChunks) fbytes
  putFilesContent rs

putPakContents :: PakState -> Put
putPakContents pak = do
  let tocSize = ((M.size $ fileMap pak) * tocEntrySize)
  let files = (M.toList $ fileMap pak)
  putByteString $ C.pack "VMPACK" -- id
  putWord32le 0 -- version
  putWord32le $ fromIntegral tocSize -- toc size in bytes
  putFilesToc files (tocSize + 14)
  putFilesContent files

main :: IO ()
main = do
  [dirname, outfile] <- getArgs
  withFile outfile WriteMode $ writePakFromDir dirname outfile
  where
    writePakFromDir dirname outfile fh = do
      pak <- processDir dirname $ newPak fh
      BL.hPut fh $ runPut (putPakContents pak)
      putStrLn ("Wrote to " ++ outfile ++ ".")
